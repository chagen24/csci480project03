#version 330 core
// Uniforms
uniform sampler2D brickNormal;
uniform sampler2D wall;

// Interpolated values from vertex shader
in vec4 normal;
in vec4 light;
in vec4 eye;
in vec2 uv;
in vec2 type;

// Ouput data
out vec4 color;

void main()
{
    float E = 0.00000001;
    vec4 basecolor = texture2D(wall, uv);
    float ambient = 0.2;
    // need to normalize interpolated vectors
    vec4 l = normalize(light);
    vec4 n = normalize(normal);
    vec4 e = normalize(eye);

    // vector needed for grahm-schmidt
    vec4 g = vec4(0,1,0,0);

    // If tower or wall, we normal-map
    if (type.x == 2 || type.x == 1) {

	// capture normal-map texture
	vec4 normalMap = normalize(texture2D(brickNormal, uv));
	vec4 bump = normalMap - vec4(0,0,1,0);
	// make sure normal isn't vertical
	if ((abs(normalMap.x) < E) && ((abs(normalMap.y)-1) < E) && (abs(normalMap.z) < E)) {
	    normalMap = vec4(0,0,1,0);
	}

        // Make sure normal isn't vertical
	if ((abs(n.x) < E) && ((abs(n.y)-1) < E) && (abs(n.z) < E)) {
	    g.x = 1;
	    g.y = 0;
        }
        float dp = dot(g,n);
	vec4 proj = dp * n;

	//g and t are our tangent and bitangent
	g = normalize(g - proj);
	vec3 placeholder = normalize(cross(g.xyz,n.xyz));
	vec4 h = vec4(placeholder.xyz, 0);

	n = n + (g * bump.x / 4) + (h * bump.y / 4);
    }
    //n = vec4(1,0,0,0);

    float diffuse = clamp(dot(l,n),0,1);
    float intensity = max(ambient, diffuse);
    color = basecolor * intensity;
    vec2 t = normalize(type);
    if (t.x < 0){
        color = color * t.x;
    }

    if (diffuse > 0.0) {
        float specular = pow(clamp(dot(reflect(-l,n),e),0,1), 16);
        color += vec4(specular,specular,specular,1);
    }
    color = clamp(color,0,1);
}
