# -*- coding: utf-8 -*-
"""
Connor Hagen
CS480
most code borrowed/modified from Geof Matthews

controls.py handles keyboard inputs and specifies the matrices
needed to move around our scene. We do this using a "Control"
object that can be modified with keyboard/mouse input which is then
translated into the correct transformation matrices which are then
passed to the shader to modify our triangles
"""

import pygame
from pygame.locals import *

from transforms import *

#initialize values for control
class Control():
    def __init__(self,
                 pos = vec((5,4,5)),
                 fwd = vec((-5,-4,-5)), 
                 up = vec((0,1,0)),
                 fov = 45.0,
                 speed=0.1,
                 mouseSpeed = 0.001):
        self.pos = pos
        self.fwd = normalize(vec(fwd))
        self.up = normalize(vec(up) - N.dot(vec(up),fwd)*fwd)
        self.rt = N.cross(fwd,up)
        self.fov = fov
        self.speed = speed
        self.mouseSpeed = mouseSpeed
        self.computeRotation()
        self.modelPosition = vec((0,0,0))
        self.modelTranslation = N.identity(4,dtype=N.float32)
        self.modelRotation = 0.0
        self.modelPitch = 0.0
        self.light = normalize(vec((5,5,1,0)))

    #handles zoom-in/zoom-out
    def handleMouseButton(self, button):
        if button == 4:
            self.fov *= 1.1
        elif button == 5:
            self.fov *= 0.9

    #handles click-drag motions
    def handleMouseMotion(self):
        pressed = pygame.mouse.get_pressed()
        if pressed[0]:
            self.computeRotation()
        elif pressed[2]:
            self.computeModelRotation()

    def computeModelRotation(self):
        speed = 0.01
        r,u = pygame.mouse.get_rel()
        r = min(r,20)
        r = max(r,-20)
        self.modelRotation -= r*0.005
        u = min(u,20)
        u = max(u,-20)
        self.modelPitch += u*0.005
        
    def computeRotation(self):       
        fwd = self.fwd
        rt = self.rt
        up = self.up
        r,u = pygame.mouse.get_rel()
        r = min(r,20)
        r = max(r,-20)
        u = min(u,20)
        u = max(u,-20)
        fwd += r*rt*self.mouseSpeed
        fwd += -u*up*self.mouseSpeed
        fwd = normalize(fwd)
        if fwd[0] != 0 or fwd[1] != 1 or fwd[2] != 0:
            up = vec((0,1,0))
        up = normalize(up - N.dot(up,fwd)*fwd)
        rt = N.cross(fwd,up)
        self.fwd = fwd
        self.rt = rt
        self.up = up

    #finds the keys pressed and adjusts our control attributes accordingly
    def handleKeyboard(self):
        pressed = pygame.key.get_pressed()
        #WASD for forward/backward right/left
        if pressed[K_w]:
            self.pos += self.speed*self.fwd
        elif pressed[K_s]:
            self.pos -= self.speed*self.fwd
        elif pressed[K_a]:
            self.pos -= self.speed*self.rt
        elif pressed[K_d]:
            self.pos += self.speed*self.rt
        #Arrow keys move model. Pageup/Pagedown move model up and down
        elif pressed[K_LEFT]:
            self.modelPosition[0] += self.speed
        elif pressed[K_RIGHT]:
            self.modelPosition[0] -= self.speed
        elif pressed[K_PAGEDOWN]:
            self.modelPosition[1] += self.speed
        elif pressed[K_PAGEUP]:
            self.modelPosition[1] -= self.speed
        elif pressed[K_UP]:
            self.modelPosition[2] += self.speed
        elif pressed[K_DOWN]:
            self.modelPosition[2] -= self.speed
        #equal key and minus key change speed of movement
        elif pressed[K_EQUALS]:
            self.speed += 0.002
        elif pressed[K_MINUS]:
            self.speed -= 0.002
        #9 key and 0 key rotate light source
        elif pressed[K_9]:
            self.light = N.dot(rotationYMatrix(N.pi/12),self.light)
        elif pressed[K_0]:
            self.light = N.dot(rotationYMatrix(-N.pi/12),self.light)
            
    #get a projection matrix based on current field of view        
    def getProjectionMatrix(self):
        self.fov = min(self.fov, 170)
        self.fov = max(self.fov, 5)
        return perspective(self.fov, 4.0/3.0, .1, 200.0)
    
    #get view matrix based on current position, foward vector
    #right vector, and up vector    
    def getViewMatrix(self):
        return viewFromFrame(self.pos, self.fwd, self.rt, self.up)

    #get model matrix
    def getModelMatrix(self):
        #insert current model position into translation matrix
        self.modelTranslation[0:3,3] = self.modelPosition
        #create matrix for rotation around y axis
        rot = rotationYMatrix(self.modelRotation)
        #create matrix for rotation around x axis
        pitch = rotationXMatrix(self.modelPitch)
        #combine x/y axis rotations
        rotpitch = N.dot(pitch, rot)
        trans = self.modelTranslation
        #combine rotation with translation
        return N.dot(trans,rotpitch)
    
    
    
                  
