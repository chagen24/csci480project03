#version 330 core

// Input vertex buffer data
// Each execution of this shader gets one vertex from the buffer
in vec4 positionBuffer;
in vec4 normalBuffer;
in vec2 uvBuffer;
in vec2 typeBuffer;

// Uniforms
// All executions of this shader get the same value for these
uniform mat4 M; // model
uniform mat4 V; // view
uniform mat4 P; // projection
uniform vec4 lightDirection;
uniform float time;

// Values interpolated for fragment shader
out vec4 normal;
out vec4 light;
out vec4 eye;
out vec2 uv;
out vec4 position;
out vec4 positionWorld;
out vec4 positionCamera;
// Our type value doesn't get interpolated
flat out vec2 type;

void main(){
    //pass interpolated uv and position
    uv = uvBuffer;
    position = positionBuffer;

    //type won't get interpolated
    type = typeBuffer;

    //calculate world position by multiplying by model matrix
    positionWorld = M * position;

    //calculate camera position by multiplying model-position by view matrix
    positionCamera = V * positionWorld;

    //calculate light by multiplying light vector by view matrix
    light = V * lightDirection;
    normal =  V * M * normalBuffer;
    eye = -(V * M * positionBuffer);

    //check to see if flag. if so, move vertices according to flag algorithm
    if (type.x == 4) {
	float offset = position.x*0.1*sin((-time+position.x+0.2*position.y)*10);
	position.z += offset;
	normal.x += offset;
    }

    gl_Position = P * V * M * position;
}

