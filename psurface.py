import numpy as N
from transforms import *

def pSurface(point, normal, texture, srange, trange, typ):
    sinc = srange[1] - srange[0]
    tinc = trange[1] - trange[0]
    verts = []
    for s in srange:
        for t in trange:
            p00 = point(s,t)
            p01 = point(s,t+tinc)
            p10 = point(s+sinc, t)
            p11 = point(s+sinc, t+tinc)
            
            n00 = normal(s,t)
            n01 = normal(s,t+tinc)
            n10 = normal(s+sinc, t)
            n11 = normal(s+sinc, t+tinc)
            
            t00 = texture(s,t)
            t01 = texture(s,t+tinc)
            t10 = texture(s+sinc, t)
            t11 = texture(s+sinc, t+tinc)

            verts.extend(p00+n00+t00)
            verts.extend(typ)
            verts.extend(p10+n10+t10)
            verts.extend(typ)
            verts.extend(p01+n01+t01)
            verts.extend(typ)
            
            verts.extend(p10+n10+t10)
            verts.extend(typ)
            verts.extend(p11+n11+t11)
            verts.extend(typ)
            verts.extend(p01+n01+t01)
            verts.extend(typ)
    return N.array(verts, dtype=N.float32)

def triangle():
    return N.array([-1,-1,0,1,
                    0,0,1,0,
                    0,0,
                    1,-1,0,1,
                    0,0,1,0,
                    1,0,
                    0,1,0,1,
                    0,0,1,0,
                    .5,1], dtype=N.float32)


#SPHERE PARAMETIZATION:

#finds sphere point based on radius, longitude and latitude
def spherePoint(radius, longangle, latangle):
    clat = N.cos(latangle)
    slat = N.sin(latangle)
    clong = N.cos(longangle)
    slong = N.sin(longangle)
    x = radius*clong*clat
    y = radius*slat
    z = -radius*slong*clat
    return [x,y,z,1.0] # return homogeneous point

#finds sphere normal based on radius, longitude and latitude
def sphereNormal(radius, longangle, latangle):
    x,y,z,w = spherePoint(radius, longangle, latangle)
    norm = normalize((x,y,z))
    return [norm[0],norm[1],norm[2], 0.0] # return homogeneous vector

#finds texture uv based on sphere point
def sphereTexture(radius, longangle, latangle):
    return [0.5*longangle/N.pi, 0.5*latangle/N.pi]

#returns a sphere according to inputs
def sphere(radius, nlongs, nlats, typ):
    sinc = 2.0*N.pi/nlongs
    srange = N.arange(0.0, 2.0*N.pi, sinc)
    tinc = N.pi/nlats
    trange = N.arange(-0.5*N.pi, 0.5*N.pi, tinc)
    return pSurface(lambda s,t:spherePoint(radius,s,t),
                    lambda s,t:sphereNormal(radius,s,t),
                    lambda s,t:sphereTexture(radius,s,t),
                    srange, trange, typ)
            

#SECTIONED FLAT CIRCLE PARAMETIZATION:

#returns a sectioned flat circle according to inputs
def seccircle(rb, rl, n, pylons, height, typ):
    """the increment of s"""
    sinc = 2.0*N.pi/(pylons*2*n)
    """the range of s"""
    srange = N.arange(0.0, 2.0*N.pi/(pylons*2), sinc)
    """normal vector"""
    norm = normalize((0.0,1.0,0.0,0.0))
    verts= []
    for p in range(0,pylons):
        for s in srange:
            p00 = [rl*N.cos(s),height,rl*N.sin(s),1.0]
            p01 = [rb*N.cos(s),height,rb*N.sin(s),1.0]
            p10 = [rb*N.cos(s+sinc),height,rb*N.sin(s+sinc),1.0]
            p11 = [rl*N.cos(s+sinc),height,rl*N.sin(s+sinc),1.0]
            
            t00 = [0,rl]
            t01 = [0,rb]
            t10 = [sinc,rb]
            t11 = [sinc,rl]
                       
            
            verts.extend(p00)
            verts.extend(norm)
            verts.extend(t00)
            verts.extend(typ)
            verts.extend(p01)
            verts.extend(norm)
            verts.extend(t01)
            verts.extend(typ)
            verts.extend(p10)
            verts.extend(norm)
            verts.extend(t10)
            verts.extend(typ)
            
            verts.extend(p10)
            verts.extend(norm)
            verts.extend(t10)
            verts.extend(typ)
            verts.extend(p11)
            verts.extend(norm)
            verts.extend(t11)
            verts.extend(typ)
            verts.extend(p00)
            verts.extend(norm)
            verts.extend(t00)
            verts.extend(typ)
        
        srange = srange + (2*N.pi/pylons)
    return N.array(verts, dtype=N.float32)
    
#returns a sectioned flat circle according to inputs
def seccirclehelper(rb, rl, n, pylons, height, typ):
    """the increment of s"""
    sinc = 2.0*N.pi/(pylons*2*n)
    """the range of s"""
    srange = N.arange(N.pi, 2.0*N.pi, sinc)
    """normal vector"""
    norm = normalize((0.0,1.0,0.0,0.0))
    verts= []
    for p in range(0,pylons):
        for s in srange:
            p00 = [rl*N.cos(s),height,rl*N.sin(s),1.0]
            p01 = [rb*N.cos(s),height,rb*N.sin(s),1.0]
            p10 = [rb*N.cos(s+sinc),height,rb*N.sin(s+sinc),1.0]
            p11 = [rl*N.cos(s+sinc),height,rl*N.sin(s+sinc),1.0]
            
            t00 = [0,rl]
            t01 = [0,rb]
            t10 = [sinc,rb]
            t11 = [sinc,rl]
                       
            
            verts.extend(p00)
            verts.extend(norm)
            verts.extend(t00)
            verts.extend(typ)
            verts.extend(p01)
            verts.extend(norm)
            verts.extend(t01)
            verts.extend(typ)
            verts.extend(p10)
            verts.extend(norm)
            verts.extend(t10)
            verts.extend(typ)
            
            verts.extend(p10)
            verts.extend(norm)
            verts.extend(t10)
            verts.extend(typ)
            verts.extend(p11)
            verts.extend(norm)
            verts.extend(t11)
            verts.extend(typ)
            verts.extend(p00)
            verts.extend(norm)
            verts.extend(t00)
            verts.extend(typ)
        
        srange = srange + (2*N.pi/pylons)
    return N.array(verts, dtype=N.float32)


#FLAT CIRCLE GENERATOR
def flatcircle(radius,n,height,typ):
    vert = []
    vert.extend(seccircle(radius,0,n,1,height,typ))
    vert.extend(seccirclehelper(radius,0,n,1,height,typ))
    return N.array(vert, dtype=N.float32)
    
#FLAT RING GENERATOR
def flatring(rb,rl,n,height,typ):
    vert = []
    vert.extend(seccircle(rb,rl,n,1,height,typ))
    vert.extend(seccirclehelper(rb,rl,n,1,height,typ))
    return N.array(vert, dtype=N.float32)
            
            
#CYLINDER PARAMETIZATION:                   
                   
#finds cylinder point based on longitude angle and height
def cylinderPoint(radius, longangle, height):
    clong = N.cos(longangle)
    slong = N.sin(longangle)
    x = radius*clong
    y = height
    z = radius*slong
    return [x,y,z,1.0] #returns homogenous point

#finds cylinder normal based on longitude angle and height
def cylinderNormal(radius, longangle, height):
    """calculate point on cylinder"""
    x,y,z,w = cylinderPoint(radius, longangle, height)
    """project point onto xz plane = norm"""
    norm = normalize((x,0,z))
    return [norm[0],norm[1],norm[2], 0.0] # return homogeneous vector
    
#finds texture uv for cylinder point
def cylinderTexture(radius, longangle, height):
    return [0.5*longangle/N.pi,height]                    
                    
#returns a cylinder according to inputs
def cylinder(radius, nlongs, nlats, bheight, height, typ):
    """the increment of s"""
    sinc = 2.0*N.pi/nlongs
    """the range of s"""
    srange = N.arange(0.0, 2.0*N.pi, sinc)
    """the increment of t"""
    tinc = float(height)/float(nlats)
    """the range of t"""
    trange = N.arange(bheight, bheight + height, tinc)
    return pSurface(lambda s,t:cylinderPoint(radius,s,t),
                    lambda s,t:cylinderNormal(radius,s,t),
                    lambda s,t:cylinderTexture(radius,s,t),
                    srange, trange, typ)
                    
def halfCylinder(radius, nlongs, nlats, height, typ):
    bheight = 0
    """the increment of s"""
    sinc = 2.0*N.pi/nlongs
    """the range of s"""
    srange = N.arange(0.0, N.pi, sinc)
    """the increment of t"""
    tinc = float(height)/float(nlats)
    """the range of t"""
    trange = N.arange(bheight, bheight + height, tinc)
    v = pSurface(lambda s,t:cylinderPoint(radius,s,t),
                    lambda s,t:cylinderNormal(radius,s+N.pi,t+N.pi),
                    lambda s,t:cylinderTexture(radius,s,t),
                    srange, trange, typ)
    rotmat = rotationXMatrix(-N.pi/2)
    return moveVerts(v,rotmat)
                    

def main():
    print seccircle(2, 1, 2, 4, 0)
    

if __name__ == "__main__":
    main()
