# -*- coding: utf-8 -*-
"""
Connor Hagen
CS480

scene.py will create the scene and pass back the vertex buffer
"""

from transforms import *
from controls import *
from texture import loadTexture
from shaders import loadProgram
from psurface import *
from tower import *
from wall import *
from skybox import *
from flag import *


def render():
    
    verts = []    
    
    #the towers
    t1 = tower(1.5,.3,32,8,.1,.4,.25,8,.1,.15)
    m = translationMatrix(-5,0,0)
    t1 = moveVerts(t1,m)
    verts.extend(t1)    
    
    t2 = tower(1.5,.3,32,8,.1,.4,.25,8,.1,.15)
    m = translationMatrix(5,0,0)
    t2 = moveVerts(t2,m) 
    verts.extend(t2)
    
    t3 = tower(2.5,.3,32,8,.1,.4,.25,8,.1,.15)
    m = translationMatrix(-7,0,6.5)
    t3 = moveVerts(t3,m) 
    verts.extend(t3)
    
    t4 = tower(2.5,.3,32,8,.1,.4,.25,8,.1,.15)
    m = translationMatrix(7,0,6.5)
    t4 = moveVerts(t4,m) 
    verts.extend(t4)
    
    t5 = tower(2.5,1,32,8,.15,1,.3,8,.15,.2)
    m = translationMatrix(0,0,15)
    t5 = moveVerts(t5,m) 
    verts.extend(t5)
    
    t6 = tower(1.85,.5,32,8,.15,.6,.3,8,.15,.2)
    m = translationMatrix(0,0,9)
    t6 = moveVerts(t6,m) 
    verts.extend(t6)
    
    t7 = tower(1.9,.2,32,8,.2,.3,.25,4,.1,.15)
    m = translationMatrix(-2,0,0)
    t7 = moveVerts(t7,m) 
    verts.extend(t7)
    
    t8 = tower(1.9,.2,32,8,.2,.3,.25,4,.1,.15)
    m = translationMatrix(2,0,0)
    t8 = moveVerts(t8,m) 
    verts.extend(t8)
    
    t9 = tower(1.9,.2,32,8,.2,.3,.25,4,.1,.15)
    m = translationMatrix(-2,0,3)
    t9 = moveVerts(t9,m) 
    verts.extend(t9)
    
    t10 = tower(1.9,.2,32,8,.2,.3,.25,4,.1,.15)
    m = translationMatrix(2,0,3)
    t10 = moveVerts(t10,m) 
    verts.extend(t10)
    
    t11 = tower(1.9,.2,32,8,.2,.3,.25,4,.1,.15)
    m = translationMatrix(-3.5,0,6)
    t11 = moveVerts(t11,m) 
    verts.extend(t11)
    
    t12 = tower(1.9,.2,32,8,.2,.3,.25,4,.1,.15)
    m = translationMatrix(3.5,0,6)
    t12 = moveVerts(t12,m) 
    verts.extend(t12)
    
    t13 = tower(2.5,.3,32,8,.1,.4,.25,8,.1,.15)
    m = translationMatrix(-3.5,0,10.75)
    t13 = moveVerts(t13,m) 
    verts.extend(t13)
    
    t14 = tower(2.5,.3,32,8,.1,.4,.25,8,.1,.15)
    m = translationMatrix(3.5,0,10.75)
    t14 = moveVerts(t14,m) 
    verts.extend(t14)
    
    #the gate
    gate = gateWall(2,4,.15)
    m = translationMatrix(-2,0,3)
    gate = moveVerts(gate,m)
    verts.extend(gate)
    
    #the walls
    w1 = plainWall(1.75,2.5,.15,[2,0])
    m = translationMatrix(2.2,0,0)
    w1 = moveVerts(w1,m)
    verts.extend(w1)
    
    w2 = plainWall(1.75,2.5,.15,[2,0])
    m = translationMatrix(-4.7,0,0)
    w2 = moveVerts(w2,m)
    verts.extend(w2)
    
    w3 = plainWall(1.75,2.8,.15,[2,0])
    mr = rotationYMatrix(N.pi/2)
    mt = translationMatrix(2.2,0,0)
    m = N.dot(mt,mr)
    w3 = moveVerts(w3,m)
    verts.extend(w3)
    
    w4 = plainWall(1.75,2.8,.15,[2,0])
    mr = rotationYMatrix(N.pi/2)
    mt = translationMatrix(-2,0,0)
    m = N.dot(mt,mr)
    w4 = moveVerts(w4,m)
    verts.extend(w4)
    
    w5 = windowWall(1.75,6.3,.15)
    mr = rotationYMatrix(N.pi/2+.3)
    mt = translationMatrix(-5,0,0.27)
    m = N.dot(mt,mr)
    w5 = moveVerts(w5,m)
    verts.extend(w5)
    
    w6 = windowWall(1.75,6.3,.15)
    mr = rotationYMatrix(N.pi/2-.3)
    mt = translationMatrix(5,0,0.27)
    m = N.dot(mt,mr)
    w6 = moveVerts(w6,m)
    verts.extend(w6)
    
    w7 = windowWall(2.25,11,.15)
    mr = rotationYMatrix(N.pi/2+.68)
    mt = translationMatrix(7,0,6.8)
    m = N.dot(mt,mr)
    w7 = moveVerts(w7,m)
    verts.extend(w7)
    
    w8 = windowWall(2.25,11,.15)
    mr = rotationYMatrix(N.pi/2-.68)
    mt = translationMatrix(-7,0,6.8)
    m = N.dot(mt,mr)
    w8 = moveVerts(w8,m)
    verts.extend(w8)
    
    w9 = windowWall(1.5,5,.15)
    mr = rotationYMatrix(N.pi/2)
    mt = translationMatrix(-3.5,0,6)
    m = N.dot(mt,mr)
    w9 = moveVerts(w9,m)
    verts.extend(w9)
    
    w10 = windowWall(1.5,5,.15)
    mr = rotationYMatrix(N.pi/2)
    mt = translationMatrix(3.5,0,6)
    m = N.dot(mt,mr)
    w10 = moveVerts(w10,m)
    verts.extend(w10)
    
    w11 = windowWall(1.5,7,.15)
    m = translationMatrix(-3.5,0,10.75)
    w11 = moveVerts(w11,m)
    verts.extend(w11)
    
    #the flag
    f = flag(1,.5)
    m = translationMatrix(0,3.5,9)
    f = moveVerts(f,m)
    verts.extend(f)
    
    pole = cylinder(.05,4,8,1.5,2,[4,1])
    m = translationMatrix(0,.5,9)
    pole = moveVerts(pole,m)
    verts.extend(pole)
    
    
    return N.array(verts, dtype=N.float32) 