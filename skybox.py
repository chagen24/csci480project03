# -*- coding: utf-8 -*-
"""
Connor Hagen
CS480

skybox.py will generate the skybox my castle dwells in
"""

import numpy as N

def skybox(w,h,d):
    b,l,n = 0,0,0
    t,r,f = h,w,d
    
    btyp = [3,0]
    ftyp = [3,1]
    ttyp = [3,2]
    ltyp = [3,3]
    rtyp = [3,4]
    typ = [0,0]
        
    #calculate corners
    bln = [b,l,n,1]
    blf = [b,l,f,1]
    brn = [b,r,n,1]
    brf = [b,r,f,1]
    tln = [t,l,n,1]
    tlf = [t,l,f,1]
    trn = [t,r,n,1]
    trf = [t,r,f,1]
    
    #calculate normals
    nfront = [0,0,-1,0]
    nback = [0,0,1,0]
    ntop = [0,1,0,0]
    nbot = [0,-1,0,0]
    nright = [1,0,0,0]
    
    #assemble box with proper texture types
    front1 = bln+nfront+[w,0]+ftyp + brn+nfront+[w,h]+ftyp + tln+nfront+[0,0]+ftyp 
    front2 = brn+nfront+[w,h]+ftyp + trn+nfront+[0,h]+ftyp + tln+nfront+[0,0]+ftyp
    top1 = tln+ntop+[w,0]+rtyp + trn+ntop+[w,d]+rtyp + tlf+ntop+[0,0]+rtyp 
    top2 = trn+ntop+[w,d]+rtyp + trf+ntop+[0,d]+rtyp + tlf+ntop+[0,0]+rtyp
    right1 = brn+nright+[0,0]+ttyp + brf+nright+[d,0]+ttyp + trf+nright+[d,h]+ttyp
    right2 = brn+nright+[0,0]+ttyp + trf+nright+[d,h]+ttyp + trn+nright+[0,h]+ttyp
    bot1 = bln+nbot+[0,0]+ltyp + blf+nbot+[w,0]+ltyp + brn+nbot+[0,d]+ltyp
    bot2 = brn+nbot+[0,d]+ltyp + blf+nbot+[w,0]+ltyp + brf+nbot+[w,d]+ltyp
    back1 = blf+nback+[0,0]+btyp + tlf+nback+[w,0]+btyp + trf+nback+[w,h]+btyp  
    back2 = blf+nback+[0,0]+btyp + trf+nback+[w,h]+btyp + brf+nback+[0,h]+btyp
    
    
    buff = front1+front2+top1+top2+right1+right2+bot1+bot2+back1+back2
    
    return N.array(buff, dtype=N.float32)