# -*- coding: utf-8 -*-
"""
Connor Hagen
CS480

tower.py will create  castle tower based on input
"""

import numpy as N
from transforms import *
from psurface import *

#TOWER CAP TRANSITION PARAMETIZATION

#finds a tower cap transition point based on taper, longitude, height, and tower height           
def tcaptranPoint(brad, taper, longangle, height, towheight):
    clong = N.cos(longangle)
    slong = N.sin(longangle)
    x = (taper*height+brad)*clong
    y = towheight + height
    z = (taper*height+brad)*slong
    return [x,y,z,1.0]        
    
#finds a tower cap transition normal based on taper, longitude, and height
def tcaptranNormal(brad, taper, longangle, height, towheight):
    """calculate point on tcaptran"""
    x,y,z,w = tcaptranPoint(brad, taper, longangle, height, towheight)
    """project point onto xz plane = norm"""
    norm = normalize((x,0,z))
    return [norm[0],norm[1],norm[2], 0.0] # return homogeneous vector
    
#finds texture uv for tower cap transition point
def tcaptranTexture(brad, taper, longangle, height, towheight):
    return [0.5*longangle/N.pi,height+towheight]
           
#returns a tower cap transition according to inputs           
def tcaptran(brad, trad, nlongs, nlats, height, towheight):        
    """the increment of s"""
    sinc = 2.0*N.pi/nlongs
    """the range of s"""
    srange = N.arange(0.0, 2.0*N.pi, sinc)
    """the increment of t"""
    tinc = float(height)/float(nlats)
    """the range of t"""
    trange = N.arange(0.0, height, tinc)
    """find taper constant"""
    taper = (float(trad)-float(brad))/float(height)
    return pSurface(lambda s,t:tcaptranPoint(brad,taper,s,t,towheight),
                    lambda s,t:tcaptranNormal(brad,taper,s,t,towheight),
                    lambda s,t:tcaptranTexture(brad,taper,s,t,towheight),
                    srange, trange, [1,0])
                    
                    
#TOWER PYLON PARAMETIZATION
                                                    
#returns tower pylon outside/inside walls according to inputs
def pylonoiwalls(radius, nlongs, nlats, height, pylons, theight):
    """the increment of s"""
    sinc = 2.0*N.pi/nlongs
    """the range of s"""
    srange = N.arange(0.0, 2.0*N.pi/(pylons*2), sinc)
    """the increment of t"""
    tinc = float(height)/float(nlats)
    """the range of t"""
    trange = N.arange(theight, theight + height, tinc)
    
    verts = []
    for p in range(0,pylons):
        verts.extend(pSurface(lambda s,t:cylinderPoint(radius,s,t),
                    lambda s,t:cylinderNormal(radius,s,t),
                    lambda s,t:cylinderTexture(radius,s,t),
                    srange, trange, [1,0]))
        srange = srange + (2*N.pi/pylons)
    
    return N.array(verts, dtype=N.float32)
    
#returns tower pylon side walls according to inputs
def pylonswalls(rl, thick, yoff, height, pylons):
    """the increment of s"""
    sinc = 2.0*N.pi/(pylons*2)
    """the range of s"""
    srange = N.arange(0.0, 2.0*N.pi, sinc)
    verts = []
    rb = rl + thick
    typ = [1,0]

    for s in srange:
        p00 = [rl*N.cos(s),yoff,rl*N.sin(s),1.0]
        p01 = [rb*N.cos(s),yoff,rb*N.sin(s),1.0]
        p10 = [rb*N.cos(s),yoff+height,rb*N.sin(s),1.0]
        p11 = [rl*N.cos(s),yoff+height,rl*N.sin(s),1.0]
        
        v1 = N.delete((N.array(p01)-N.array(p00)),3)
        v2 = N.delete((N.array(p11)-N.array(p00)),3)
        n = normalize(N.cross(v1,v2))
        n = N.append(n,0)
        
        t00 = [0,0]
        t01 = [thick,0]
        t10 = [thick,height]
        t11 = [0,height]        
        
        verts.extend(p00)
        verts.extend(n)
        verts.extend(t00)
        verts.extend(typ)
        verts.extend(p01)
        verts.extend(n)
        verts.extend(t01)
        verts.extend(typ)
        verts.extend(p10)
        verts.extend(n)
        verts.extend(t10)
        verts.extend(typ)
        
        verts.extend(p10)
        verts.extend(n)
        verts.extend(t10)
        verts.extend(typ)
        verts.extend(p11)
        verts.extend(n)
        verts.extend(t11)
        verts.extend(typ)
        verts.extend(p00)
        verts.extend(n)
        verts.extend(t00)
        verts.extend(typ)

    return N.array(verts, dtype=N.float32)
    

#TOWER CONSTRUCTION

#returns a complete tower with the following parameters:
#base height, base radius, longitude numbers, lattitude number,
#transition height, top radius, 
#top height,
#number of pylons, pylon height, wall thickness
    
def tower(bheight,brad,nlongs,nlat,
          transheight,trad,theight,
          pylons,pheight,thick):
    typ = [1,0]
    verts = []
    """base of tower"""
    verts.extend(cylinder(brad,nlongs,nlat,0,bheight,typ))
    """transition piece"""
    verts.extend(tcaptran(brad,trad,nlongs,nlat,transheight,bheight))
    """top outter cylinder"""
    verts.extend(cylinder(trad,nlongs,nlat,bheight+transheight,theight,typ))
    """top inner cylinder"""
    verts.extend(cylinder(trad-thick,nlongs,nlat,bheight+transheight,theight,typ))
    """top cylinder cap"""
    verts.extend(flatring(trad,trad-thick,nlongs,bheight+transheight+theight,typ))
    """top floor"""
    verts.extend(flatcircle(trad,nlongs,bheight+transheight,typ))
    """pylon inner walls"""
    verts.extend(pylonoiwalls(trad-thick, nlongs, nlat, pheight-.01, pylons, theight+transheight+bheight))
    """pylon outer walls"""
    verts.extend(pylonoiwalls(trad, nlongs, nlat, pheight-.01, pylons, theight+transheight+bheight))
    """top of pylons"""
    verts.extend(seccircle(trad,trad-thick,8,pylons,pheight+theight+transheight+bheight,typ))
    """pylon side walls"""
    verts.extend(pylonswalls(trad-thick, thick, theight+transheight+bheight, pheight, pylons))
    
    return N.array(verts, dtype=N.float32)