# -*- coding: utf-8 -*-

"""import essentials for opengl, pygame, and numpy"""
import os
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GL.shaders import compileShader, compileProgram
import pygame
from pygame.locals import *
import numpy as N
from ctypes import c_void_p

"""import bits I've created and need"""
from transforms import *
from controls import *
from texture import loadTexture
from shaders import loadProgram
from psurface import *
from tower import *
from wall import *
from skybox import *
from flag import *
from scene import *

def main ():
    #initialize pygame
    pygame.init()
    """sets display size/buffer type"""
    screen = pygame.display.set_mode((1024,768), OPENGL|DOUBLEBUF)
    """initialize clock"""
    clock = pygame.time.Clock()
    
    #initialize opengl
    """the color to clear buffer"""
    glClearColor(0.0,0.0,0.4,0.0)
    """does depth comparisons and updates the depth buffer"""
    glEnable(GL_DEPTH_TEST)
    """passes over the item if the depth value is less than stored depth value"""
    glDepthFunc(GL_LESS)
    
    #create the vertex array to be passed to shaders
    VertexArrayID = glGenVertexArrays(1)
    """signifies which vertex array we're talking about"""
    glBindVertexArray(VertexArrayID)
    
    #compile shaders with shader.py
    programID = loadProgram(
        os.path.join("shaders","shaderexp.vert"),
        os.path.join("shaders","shaderexp.frag")
        )
        
    #get handles for "MVP" uniform
    ViewMatrixID = glGetUniformLocation(programID, "V")
    ModelMatrixID = glGetUniformLocation(programID, "M")
    ProjectionMatrixID = glGetUniformLocation(programID, "P")
    print "P,V,M:",ProjectionMatrixID,ViewMatrixID,ModelMatrixID
    
    #load textures here
    TextureBrickTex = loadTexture(os.path.join("images","bricktex.jpg"))
    TextureBrickBump = loadTexture(os.path.join("images","bricknorm.jpg"))
    TextureGateDiscard = loadTexture(os.path.join("images","gatediscard.jpg"))
    TextureSkyTop = loadTexture(os.path.join("images","skytop.jpg"))
    TextureSkyRight = loadTexture(os.path.join("images","skyright.jpg"))
    TextureSkyLeft = loadTexture(os.path.join("images","skyleft.jpg"))
    TextureSkyFront = loadTexture(os.path.join("images","skyfront.jpg"))
    TextureSkyBack = loadTexture(os.path.join("images","skyback.jpg"))
    TextureFlagDiscard = loadTexture(os.path.join("images","flagdiscard.jpg"))
    TextureFlag = loadTexture(os.path.join("images","flag.jpg"))
    TextureWinDiscard = loadTexture(os.path.join("images","windowdiscard.jpg"))
    
    #get a handle for texture sampler
    TextureID01 = glGetUniformLocation(programID, "wall")
    TextureID02 = glGetUniformLocation(programID, "brickNormal")
    TextureID03 = glGetUniformLocation(programID, "gateDiscard")
    TextureID04 = glGetUniformLocation(programID, "skyTop")
    TextureID05 = glGetUniformLocation(programID, "skyRight")
    TextureID06 = glGetUniformLocation(programID, "skyLeft")
    TextureID07 = glGetUniformLocation(programID, "skyFront")
    TextureID08 = glGetUniformLocation(programID, "skyBack")
    TextureID09 = glGetUniformLocation(programID, "flagDiscard")
    TextureID10 = glGetUniformLocation(programID, "flag")
    TextureID11 = glGetUniformLocation(programID, "windowDiscard")
    print "TextureBrickTex, TextureBump, TextureGateDiscard, TextureWinDiscard:", TextureID01, TextureID02, TextureID03, TextureID11
    print "SkyTextures:", TextureID04, TextureID05, TextureID06, TextureID07, TextureID08
    print "TextureFlagDiscard, TextureFlag:", TextureID09, TextureID10
    
    
    #LOAD ALL OBJECTS HERE
    # vertex, normal, and texture uv data:
    #vertNormUVData = sphere(1,16,8)
    #vertNormUVTypeData = cylinder(1, 16, 8, 0, 1, [1,0])
    #vertNormUVData = tcaptran(1,2,16,16,1,1)
    #vertNormUVData = seccircle(2, 1, 8, 4, 0)
    #vertNormUVData = seccircle(2, 0, 8, 1, 0)
    #vertNormUVData = seccirclehelper(2, 0, 8, 1, 0)
    #vertNormUVData = flatcircle(2,8,0)
    #vertNormUVData = pylonoiwalls(1, 16, 16, 1, 4)
    #vertNormUVData = pylonswalls(1, 1, 0, 1, 4)
    #bheight,brad,nlongs,bnlat,transheight,transnlat,trad,theight,topnlat,
    #pylons,pnlat,pheight,thick
    #vertNormUVTypeData = tower(1,.25,32,8,.1,.33,.25,8,.1,.15)
    #vertNormUVTypeData = plainWall(1,4,.5,[2,0])
    #vertNormUVTypeData = halfCylinder(1, 12, 6, 2, [1,0])
    #vertNormUVTypeData = gateWall(3,4,1)
    #vertNormUVTypeData = windowWall(3,10,.5)
    #vertNormUVTypeData = skybox(10,10,10)
    #vertNormUVTypeData = flag(1,.5)
    vertNormUVTypeData = render()


    #total number of raw vertices
    numberOfVertices = len(vertNormUVTypeData)/(4+4+2+2)
    #calculate and display number of trianlges produced
    print "N Triangles:", numberOfVertices/3
    
    #create an openGL buffer 
    vertNormUVTypeBuffer = glGenBuffers(1)
    #specifies the target is an array of vertices
    glBindBuffer(GL_ARRAY_BUFFER, vertNormUVTypeBuffer)
    #creates and initializes a buffer object's data store
    glBufferData(GL_ARRAY_BUFFER, vertNormUVTypeData, GL_STATIC_DRAW)


    #create the light uniform
    LightID = glGetUniformLocation(programID, "lightDirection")
    print "LightID:", LightID
    
    #create time uniform
    TimeID = glGetUniformLocation(programID, "time")
    print "TimeID:", TimeID

    #get handles for our shader input varibles
    PositionID = glGetAttribLocation(programID, "positionBuffer")
    NormalID = glGetAttribLocation(programID, "normalBuffer")
    uvID = glGetAttribLocation(programID, "uvBuffer")
    typeID = glGetAttribLocation(programID, "typeBuffer")
    print "pos, norm, uv, type:", PositionID, NormalID, uvID, typeID
    
    #create our control object from an eye position and focus
    eye = vec((0,19,-10))
    focus = vec((0,0,9))
    control = Control(pos=eye,
                      fwd=focus-eye)
    print control.pos
    print control.fwd
     
    #set running to true                 
    running = True
    
    #initialize time
    time = 0.0    
    
    #actual loop for rendering with controls
    while running:
        #set framerate to 30fps
        clock.tick(30)
        #increment time
        time += 0.03333
        
        #check for exit commands or zoom
        for event in pygame.event.get():
            if event.type == QUIT:
                running = False
            if event.type == KEYUP and event.key == K_ESCAPE:
                running = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                control.handleMouseButton(event.button)
                
        #handle all the buttons pressed to compute new MVP matrix
        control.handleMouseMotion()
        control.handleKeyboard()

        #compute MVP
        ProjectionMatrix = control.getProjectionMatrix()
        ViewMatrix = control.getViewMatrix()
        ModelMatrix = control.getModelMatrix()

        #draw into opengl context
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        #use our shader
        glUseProgram(programID)
        
        # send our transform matrices to the shader
        glUniformMatrix4fv(ProjectionMatrixID, 1, GL_TRUE, ProjectionMatrix)
        glUniformMatrix4fv(ModelMatrixID, 1, GL_TRUE, ModelMatrix)
        glUniformMatrix4fv(ViewMatrixID, 1, GL_TRUE, ViewMatrix)
        # GL_TRUE: transpose the matrix for opengl column major order

        #define our light vector
        lightDirection = control.light
        #send our light vector to shader
        glUniform4fv(LightID, 1, lightDirection)
        
        #define our light vector
        lightDirection = control.light
        #send our light vector to shader
        glUniform4fv(LightID, 1, lightDirection)
        
        #send the current time
        glUniform1f(TimeID, time)
        
        #LOAD TEXTURES HERE
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, TextureBrickTex)
        glUniform1i(TextureID01, 0)
        
        glActiveTexture(GL_TEXTURE1)
        glBindTexture(GL_TEXTURE_2D, TextureBrickBump)
        glUniform1i(TextureID02, 1)
        
        glActiveTexture(GL_TEXTURE2)
        glBindTexture(GL_TEXTURE_2D, TextureGateDiscard)
        glUniform1i(TextureID03, 2)
        
        glActiveTexture(GL_TEXTURE3)
        glBindTexture(GL_TEXTURE_2D, TextureSkyTop)
        glUniform1i(TextureID04, 3)
        
        glActiveTexture(GL_TEXTURE4)
        glBindTexture(GL_TEXTURE_2D, TextureSkyRight)
        glUniform1i(TextureID05, 4)
        
        glActiveTexture(GL_TEXTURE5)
        glBindTexture(GL_TEXTURE_2D, TextureSkyLeft)
        glUniform1i(TextureID06, 5)
        
        glActiveTexture(GL_TEXTURE6)
        glBindTexture(GL_TEXTURE_2D, TextureSkyFront)
        glUniform1i(TextureID07, 6)
        
        glActiveTexture(GL_TEXTURE7)
        glBindTexture(GL_TEXTURE_2D, TextureSkyBack)
        glUniform1i(TextureID08, 7)
        
        glActiveTexture(GL_TEXTURE8)
        glBindTexture(GL_TEXTURE_2D, TextureFlagDiscard)
        glUniform1i(TextureID09, 8)
        
        glActiveTexture(GL_TEXTURE9)
        glBindTexture(GL_TEXTURE_2D, TextureFlag)
        glUniform1i(TextureID10, 9)
        
        glActiveTexture(GL_TEXTURE10)
        glBindTexture(GL_TEXTURE_2D, TextureWinDiscard)
        glUniform1i(TextureID11, 10)
        
        #make sure windows only repeat in the width
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER)

        #only one attribute buffer:
        glBindBuffer(GL_ARRAY_BUFFER, vertNormUVTypeBuffer)
        
        # three different attribute pointers:
        bytesPerFloat = 4
        
        # points:
        if PositionID >= 0:
            glEnableVertexAttribArray(PositionID)
            glVertexAttribPointer(
                PositionID,             # attrib  
                4,                      # size
                GL_FLOAT,               # type
                GL_FALSE,               # normalized?
                12*bytesPerFloat,       # stride
                c_void_p(0)             # array buffer offset
                )
                
        # normals:
        if NormalID >= 0:
            glEnableVertexAttribArray(NormalID)
            glVertexAttribPointer(
                NormalID,
                4,
                GL_FLOAT,
                GL_FALSE,
                12*bytesPerFloat,
                c_void_p(4*bytesPerFloat))
                
        # uvs:
        if uvID >= 0:
            glEnableVertexAttribArray(uvID)
            glVertexAttribPointer(
                uvID,
                2,
                GL_FLOAT,
                GL_FALSE,
                12*bytesPerFloat,
                c_void_p(8*bytesPerFloat))
                
        # types:
        if typeID >= 0:
            glEnableVertexAttribArray(typeID)
            glVertexAttribPointer(
                typeID,
                2,
                GL_FLOAT,
                GL_FALSE,
                12*bytesPerFloat,
                c_void_p(10*bytesPerFloat))
        
        # draw the triangle
        glDrawArrays(GL_TRIANGLES, 0, numberOfVertices)

        if PositionID >= 0:
            glDisableVertexAttribArray(PositionID)
        if NormalID >= 0:
            glDisableVertexAttribArray(NormalID)
        if uvID >= 0:
            glDisableVertexAttribArray(uvID)
            
        glBindBuffer(GL_ARRAY_BUFFER, 0)

        # swap buffers
        pygame.display.flip()
        
    glDeleteProgram(programID)

if __name__ == '__main__':
    try:
        main()
    finally:
        pygame.quit()
