# -*- coding: utf-8 -*-

"""import opengl bits"""
import os,sys,exceptions
from OpenGL.GL import *
from OpenGL.GL.shaders import compileShader, compileProgram

#intelligently loads a file
def loadFile(filename):
    """finds the path of the given filename in current working directory"""
    with open(os.path.join(os.getcwd(), filename)) as fp:
        return fp.read()

#compiles opengl program
def loadProgram(vertexfile, fragmentfile):
    """load the vert and frag shader files"""
    vert = loadFile(vertexfile)
    frag = loadFile(fragmentfile)
    
    """compile vertex shader"""
    try:
        vertprog = compileShader(vert, GL_VERTEX_SHADER)
    except exceptions.RuntimeError as err:
        print "VERTEX SHADER ERROR:\n", err[0]
        
    """compile frag shader"""
    try:
        fragprog = compileShader(frag, GL_FRAGMENT_SHADER)
    except exceptions.RuntimeError as err:
        print "FRAGMENT SHADER ERROR:\n", err[0]
        
    """takes compiled vert and frag shader and compiles into opengl program"""
    try:
        myProgram = compileProgram(vertprog, fragprog)
    except exceptions.RuntimeError as err:
        print "SHADER LINKER ERROR:\n", err[0]
    return myProgram
        