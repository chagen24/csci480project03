#version 330 core
// Uniforms
uniform sampler2D brickNormal;
uniform sampler2D wall;
uniform sampler2D gateDiscard;
uniform sampler2D skyTop;
uniform sampler2D skyRight;
uniform sampler2D skyLeft;
uniform sampler2D skyFront;
uniform sampler2D skyBack;
uniform sampler2D flagDiscard;
uniform sampler2D flag;
uniform sampler2D windowDiscard;

// Interpolated values from vertex shader
in vec4 position;
in vec4 positionWorld;
in vec4 positionCamera;
in vec4 normal;
in vec4 light;
in vec4 eye;
in vec2 uv;

// our type value isn't interpolated
flat in vec2 type;

// Ouput data
out vec4 color;


// Noise routines

//this routine smoothly interpolates between a and b
float smerp(float a, float b, float x)
{
    x = x*2.0-1.0;
    float f = 0.5 + 0.75*x - 0.25*x*x*x;
    return mix(a,b,f);
}

//returns random integer for input n
int integerNoise(in int n)
{
    n = (n << 13) ^ n;
    return (n * (n*n*15731+789221) + 1376312589) & 0x7fffffff;
}

//returns float in range 0..1
float noise(vec3 position)
{
  int n = integerNoise(int(position.x));
  n = integerNoise(n + int(position.y));
  n = integerNoise(n + int(position.z));
  return float(n % 1000000)/1000000.0;
}

//combines the smerps of our xyz values
float interpolatedNoise(in vec3 position)
{
  float x = position.x;
  float y = position.y;
  float z = position.z;
  float intX = floor(x);
  float intY = floor(y);
  float intZ = floor(z);
  float fracX = fract(x);
  float fracY = fract(y);
  float fracZ = fract(z);
  float v000 = noise(vec3(intX  , intY  , intZ  ));
  float v001 = noise(vec3(intX  , intY  , intZ+1));
  float v010 = noise(vec3(intX  , intY+1, intZ  ));
  float v100 = noise(vec3(intX+1, intY  , intZ  ));
  float v011 = noise(vec3(intX  , intY+1, intZ+1));
  float v110 = noise(vec3(intX+1, intY+1, intZ  ));
  float v101 = noise(vec3(intX+1, intY  , intZ+1));
  float v111 = noise(vec3(intX+1, intY+1, intZ+1));
  float v1 = smerp(v011, v111, fracX);
  float v2 = smerp(v001, v101, fracX);
  float v5 = smerp(v2,v1,fracY);
  float v3 = smerp(v010,v110,fracX);
  float v4 = smerp(v000,v100,fracX);
  float v6 = smerp(v4,v3,fracY);
  return smerp(v6, v5, fracZ);
}

//combines the scaled interpolated noise values at our position
float fbmNoise(in vec3 position)
{
  float total = 0.0;
  total += interpolatedNoise(position);
  total += interpolatedNoise(2.0*position)*0.5;
  total += interpolatedNoise(4.0*position)*0.25;
  total += interpolatedNoise(8.0*position)*0.125;
  total += interpolatedNoise(16.0*position)*0.0625;
  return total/1.9375;
}



void main()
{
    // need to normalize interpolated vectors
    vec4 l = normalize(light);
    vec4 n = normalize(normal);
    vec4 e = normalize(eye);
    float ambient = 0.1;
    float diffuse = clamp(dot(l,n),0,1);
    float intensity = max(ambient, diffuse);
    vec4 basecolor = texture2D(wall, uv);
    float specular;

    //for floating point error stuff
    float E = 0.0000000001;

    //check to see if we can discard
    if (type.x == 2 && type.y == 2) {
	if (texture2D(gateDiscard, vec2(uv.x/4,uv.y/3)).x == 1) {
	    discard;
	}
    }

    if (type.x == 4 && type.y == 0) {
	if (texture2D(flagDiscard, uv-.95).x == 1) {
	    discard;
	}
    }

    if (type.x == 2 && type.y == 1) {
	if (texture2D(windowDiscard, vec2(uv.x,uv.y)).x == 1) {
	    discard;
	}
    }


    //if our object is a wall or tower, we compute the noise and normal
    if (type.x == 2 || type.x == 1) {
	//our base colors for the noise function
	vec4 basecolor1 = vec4(.67,.62,.56,1.0);
	vec4 basecolor2 = vec4(.83,.65,.46,1.0);

	//the color of our wall texture
	vec4 basecolor3 = texture2D(wall, uv);
	//the fragment's position in the world space
	vec4 pos = positionWorld;

	//calculate the noise function for our position
	float noise = fbmNoise(pos.xyz*2.0);
	
	//linearly mix the two base colors with the noise value and add the wall color
	basecolor = mix(basecolor1, basecolor2, noise) + basecolor3*.75;

	//Here we'll calculate the normal from the bump map

	// vector needed for grahm-schmidt
	vec4 g = vec4(0,1,0,0);

	//capture normal-map texture
	vec4 normalMap = normalize(texture2D(brickNormal, uv));
	vec4 bump = normalMap - vec4(0,0,1,0);

        //make sure the normal we're grahm-schmidting isn't vertical
	if ((abs(n.x) < E) && ((abs(n.y)-1) < E) && (abs(n.z) < E)) {
	    g.x = 1;
	    g.y = 0;
        }

	//the grahm-schmidt proccess
        float dp = dot(g,n);
	vec4 proj = dp * n;
	//g and t are our tangent and bitangent
	g = normalize(g - proj);
	vec3 placeholder = normalize(cross(g.xyz,n.xyz));
	vec4 h = vec4(placeholder.xyz, 0);

	//modify the normal based on bumpmap values
	n = normalize(n + (g * bump.x)*1.25 + (h * bump.y)*1.25);
    }

    //for flag type
    if (type.x == 4 && type.y == 0) {
	basecolor = texture2D(flag, vec2(uv.x,uv.y*2-.05));
    }

    //for flag pole
    if (type.x == 4 && type.y == 1) {
	basecolor = vec4(.5,.5,.5,1);
    }

    //calculate color based on intensity
    color = basecolor * intensity;


    //calculate specular if diffuse is present
    if (diffuse > 0.0) {
        specular = pow(clamp(dot(reflect(-l,n),e),0,1), 2);
    	//check to see if gate half-cylinder
    	if (type.x == 2 && type.y == 3) {
	    specular = 0;
        }

        color += vec4(specular,specular,specular,1)/4.5;
    }

    //make sure our color values are between 0 and 1
    color = clamp(color,0,1);
}
