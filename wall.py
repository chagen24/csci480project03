# -*- coding: utf-8 -*-
"""
Connor Hagen
CS480

wall.py will create castle wall based on input
"""

import numpy as N
from transforms import *
from psurface import *


#creates a plain wall
#don't listen to the variables, it actually goes: height,width,depth
def plainWall(w,h,d,typ):
    b,l,n = 0,0,0
    t,r,f = h,w,d
        
    #calculate corners
    bln = [b,l,n,1]
    blf = [b,l,f,1]
    brn = [b,r,n,1]
    brf = [b,r,f,1]
    tln = [t,l,n,1]
    tlf = [t,l,f,1]
    trn = [t,r,n,1]
    trf = [t,r,f,1]
    
    #calculate normals
    nfront = [0,0,-1,0]
    nback = [0,0,1,0]
    ntop = [0,1,0,0]
    nbot = [0,-1,0,0]
    nleft = [-1,0,0,0]
    nright = [1,0,0,0]
    
    
    front1 = bln+nfront+[h,0]+typ + brn+nfront+[h,w]+typ + tln+nfront+[0,0]+typ 
    front2 = brn+nfront+[h,w]+typ + trn+nfront+[0,w]+typ + tln+nfront+[0,0]+typ
    top1 = tln+ntop+[d,0]+typ + trn+ntop+[d,w]+typ + tlf+ntop+[0,0]+typ 
    top2 = trn+ntop+[d,w]+typ + trf+ntop+[0,w]+typ + tlf+ntop+[0,0]+typ
    left1 = bln+nleft+[0,0]+typ + tln+nleft+[h,0]+typ + tlf+nleft+[h,d]+typ 
    left2 = bln+nleft+[0,0]+typ + tlf+nleft+[h,d]+typ + blf+nleft+[0,d]+typ
    right1 = brn+nright+[0,d]+typ + brf+nright+[0,0]+typ + trf+nright+[h,0]+typ
    right2 = brn+nright+[0,d]+typ + trf+nright+[h,0]+typ + trn+nright+[h,d]+typ
    bot1 = bln+nbot+[0,0]+typ + blf+nbot+[d,0]+typ + brn+nbot+[0,w]+typ
    bot2 = brn+nbot+[0,w]+typ + blf+nbot+[d,0]+typ + brf+nbot+[d,w]+typ
    back1 = blf+nback+[0,0]+typ + tlf+nback+[h,0]+typ + trf+nback+[h,w]+typ  
    back2 = blf+nback+[0,0]+typ + trf+nback+[h,w]+typ + brf+nback+[0,w]+typ
    
    
    buff = front1+front2+top1+top2+left1+left2+right1+right2+bot1+bot2+back1+back2
    
    return N.array(buff, dtype=N.float32)

def gateWall(w,h,d):
    b,l,n = 0,0,0
    t,r,f = h,w,d
    
    typ = [2,0]
    gate = [2,2]
    
    #calculate corners
    bln = [b,l,n,1]
    blf = [b,l,f,1]
    brn = [b,r,n,1]
    brf = [b,r,f,1]
    tln = [t,l,n,1]
    tlf = [t,l,f,1]
    trn = [t,r,n,1]
    trf = [t,r,f,1]
    
    #calculate normals
    nfront = [0,0,-1,0]
    nback = [0,0,1,0]
    ntop = [0,1,0,0]
    nbot = [0,-1,0,0]
    nleft = [-1,0,0,0]
    nright = [1,0,0,0]
    
    #form walls, making sure front and back are gate type
    front1 = bln+nfront+[h,0]+gate + brn+nfront+[h,w]+gate + tln+nfront+[0,0]+gate 
    front2 = brn+nfront+[h,w]+gate + trn+nfront+[0,w]+gate + tln+nfront+[0,0]+gate
    top1 = tln+ntop+[d,0]+typ + trn+ntop+[d,w]+typ + tlf+ntop+[0,0]+typ 
    top2 = trn+ntop+[d,w]+typ + trf+ntop+[0,w]+typ + tlf+ntop+[0,0]+typ
    left1 = bln+nright+[0,0]+typ + tln+nright+[h,0]+typ + tlf+nright+[h,d]+typ 
    left2 = bln+nright+[0,0]+typ + tlf+nright+[h,d]+typ + blf+nright+[0,d]+typ
    right1 = brn+nright+[0,d]+typ + brf+nright+[0,0]+typ + trf+nright+[h,0]+typ
    right2 = brn+nright+[0,d]+typ + trf+nright+[h,0]+typ + trn+nright+[h,d]+typ
    bot1 = bln+nbot+[0,0]+typ + blf+nbot+[d,0]+typ + brn+nbot+[0,w]+typ
    bot2 = brn+nbot+[0,w]+typ + blf+nbot+[d,0]+typ + brf+nbot+[d,w]+typ
    back1 = blf+nback+[0,0]+gate + tlf+nback+[h,0]+gate + trf+nback+[h,w]+gate  
    back2 = blf+nback+[0,0]+gate + trf+nback+[h,w]+gate + brf+nback+[0,w]+gate


    #construct top part of gate. 
    #gate wall is 40 wide
    #gate itself is 10 wide
    topbit = halfCylinder(.5, 16, 8, d, [2,3])
    transmat = translationMatrix(2,1,d)
    topbit = moveVerts(topbit,transmat)
    
    #construct walls lining gate
    #right wall
    bln = [2.5,0,0,1]
    blf = [2.5,0,d,1]
    tln = [2.5,1,0,1]
    tlf = [2.5,1,d,1]
    l1 = bln+nleft+[0,0]+typ + blf+nleft+[d,0]+typ + tln+nleft+[0,1]+typ
    l2 = blf+nleft+[d,0]+typ + tlf+nleft+[d,1]+typ + tln+nleft+[0,1]+typ
    #left wall
    brn = [1.5,0,0,1]
    brf = [1.5,0,d,1]
    trf = [1.5,1,d,1]
    trn = [1.5,1,0,1]
    r1 = brn+nright+[d,0]+typ + trf+nright+[0,1]+typ + brf+nright+[0,0]+typ
    r2 = brn+nright+[d,0]+typ + trn+nright+[d,1]+typ + trf+nright+[0,1]+typ

    
    buff = front1+front2+top1+top2+left1+left2+right1+right2+bot1+bot2+back1+back2+l1+l2+r1+r2
    buff.extend(topbit)
    
    return N.array(buff, dtype=N.float32)
    

#height width depth
def windowWall(w,h,d):
    b,l,n = 0,0,0
    t,r,f = h,w,d
    
    typ = [2,0]
    window = [2,1]
    
    #calculate corners
    bln = [b,l,n,1]
    blf = [b,l,f,1]
    brn = [b,r,n,1]
    brf = [b,r,f,1]
    tln = [t,l,n,1]
    tlf = [t,l,f,1]
    trn = [t,r,n,1]
    trf = [t,r,f,1]
    
    #calculate normals
    nfront = [0,0,-1,0]
    nback = [0,0,1,0]
    ntop = [0,1,0,0]
    nbot = [0,-1,0,0]
    nleft = [-1,0,0,0]
    nright = [1,0,0,0]
    
    #form walls, making sure front and back are window type
    front1 = bln+nfront+[h,0]+window + brn+nfront+[h,w]+window + tln+nfront+[0,0]+window 
    front2 = brn+nfront+[h,w]+window + trn+nfront+[0,w]+window + tln+nfront+[0,0]+window
    top1 = tln+ntop+[d,0]+typ + trn+ntop+[d,w]+typ + tlf+ntop+[0,0]+typ 
    top2 = trn+ntop+[d,w]+typ + trf+ntop+[0,w]+typ + tlf+ntop+[0,0]+typ
    left1 = bln+nleft+[0,0]+typ + tln+nleft+[h,0]+typ + tlf+nleft+[h,d]+typ 
    left2 = bln+nleft+[0,0]+typ + tlf+nleft+[h,d]+typ + blf+nleft+[0,d]+typ
    right1 = brn+nright+[0,d]+typ + brf+nright+[0,0]+typ + trf+nright+[h,0]+typ
    right2 = brn+nright+[0,d]+typ + trf+nright+[h,0]+typ + trn+nright+[h,d]+typ
    bot1 = bln+nbot+[0,0]+typ + blf+nbot+[d,0]+typ + brn+nbot+[0,w]+typ
    bot2 = brn+nbot+[0,w]+typ + blf+nbot+[d,0]+typ + brf+nbot+[d,w]+typ
    back1 = blf+nback+[0,0]+window + tlf+nback+[h,0]+window + trf+nback+[h,w]+window  
    back2 = blf+nback+[0,0]+window + trf+nback+[h,w]+window + brf+nback+[0,w]+window
    
    buff = front1+front2+top1+top2+left1+left2+right1+right2+bot1+bot2+back1+back2    

    nwindows = int(N.floor(h))
    
    for n in range(0,nwindows):
        #create the top bit for the window
        topbit = halfCylinder(.1, 16, 8, d, [2,0])
        transmat = translationMatrix(n+.1,.9,d)
        topbit = moveVerts(topbit,transmat)
        buff.extend(topbit)
        #create the side walls for window
        #right wall
#        bln = [1.25+n*1.25,1,0,1]
#        blf = [1.25+n*1.25,1,d,1]
#        tln = [1.25+n*1.25,1.35,0,1]
#        tlf = [1.25+n*1.25,1.35,d,1]
#        l1 = bln+nleft+[0,0]+typ + blf+nleft+[d,0]+typ + tln+nleft+[0,1]+typ
#        l2 = blf+nleft+[d,0]+typ + tlf+nleft+[d,1]+typ + tln+nleft+[0,1]+typ
#        #left wall
#        brn = [1+n*1.25,1,0,1]
#        brf = [1+n*1.25,1,d,1]
#        trf = [1+n*1.25,1.35,d,1]
#        trn = [1+n*1.25,1.35,0,1]
#        r1 = brn+nright+[d,0]+typ + trf+nright+[0,1]+typ + brf+nright+[0,0]+typ
#        r2 = brn+nright+[d,0]+typ + trn+nright+[d,1]+typ + trf+nright+[0,1]+typ
#        buff.extend(l1)
#        buff.extend(l2)
#        buff.extend(r1)
#        buff.extend(r2)
        
    return N.array(buff, dtype=N.float32)

def main():
    pass

if __name__ == "__main__":
    main()