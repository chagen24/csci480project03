# -*- coding: utf-8 -*-
"""
Connor Hagen
CS480

flag.py will create a flag to be sent to the shader
"""

import numpy as N

def flag(w,h):
    ix = float(w)/10
    iy = float(h)/10
    n = [0,1,0,0]
    typ = [4,0]
    verts = []

    
    for i in range(0,9):
        for j in range(0,9):
            p00 = [ix*j, iy*i, 0, 1]
            p01 = [ix*j+ix, iy*i, 0, 1]
            p11 = [ix*j+ix, iy*i+iy, 0, 1]
            p10 = [ix*j, iy*i+iy, 0, 1]
        
            t00 = [ix*j, iy*i]
            t01 = [ix*j+ix, iy*i]
            t11 = [ix*j+ix, iy*i+iy]
            t10 = [ix*j, iy*i+iy]
        
            left = p00+n+t00+typ+p11+n+t11+typ+p10+n+t10+typ
            right = p00+n+t00+typ+p01+n+t01+typ+p11+n+t11+typ
        
            verts.extend(left)
            verts.extend(right)
    print verts[0:100] 
        
    return N.array(verts, dtype=N.float32)