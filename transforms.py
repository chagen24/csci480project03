"""
Connor Hagen
CS480
most code borrowed/modified from Geof Matthews

transforms.py is mainly used to create the several
matrices that control movement, placement, and 
correct projection of objects in the world
"""

import numpy as N
from OpenGL.GLU import *

#return numpy vector
def vec(ns):
    return N.array(ns, dtype=N.float32)

#normalizes vector
def normalize(vec):
    vec = N.array(vec, dtype=N.float32)
    mag = N.sqrt(N.dot(vec,vec))
    return vec/mag

#creates fresh homogenous zero matrix
def newMatrix():
    return N.zeros((4,4),dtype=N.float32)

#sets the first matrix equl to second
def setMatrix(m1, m2):
    m1[:,:] = m2

#creates a projection matrix depending on inputs:
#near clipping plane, far clipping plane, width, height
def projectionMatrix(n,f,w,h):
    return N.array(((2.0*n/w, 0, 0, 0),
                    (0, 2.0*n/h, 0, 0),
                    (0, 0, -(f+n)/(f-n), -2.0*f*n/(f-n)),
                    (0, 0, -1, 0)), dtype = N.float32)

#sets projection matrix
def setProjection(m,n,f,w,h):
    m[:,:] = 0.0
    m[0,0] = 2.0*n/w
    m[1,1] = 2.0*n/h
    m[2,2] = -(f+n)/(f-n)
    m[2,3] = -2.0*f*n/(f-n)
    m[3,2] = -1.0

#creates projection matrix based on following inputs:
#field of view angle for height, aspect ratio, near clipping-plane, far clipping plane
def perspective(fovy, ar, n, f):
    h = 2.0*n*N.tan(0.5*N.pi*fovy/180.0)
    w = ar*h
    return projectionMatrix(n, f, w, h)

#returns a view matrix from inputs:
#camera position, point to look at, up vector
def lookAt(cameraAt, lookAt, up):
    cameraAt = vec(cameraAt)
    trans = N.identity(4,dtype=N.float32)
    trans[0:3,3] = -cameraAt
    lookAt = vec(lookAt)
    fwd = normalize(lookAt - cameraAt)
    up = normalize(up)
    up = up - N.dot(up,fwd)*fwd
    rt = normalize(N.cross(fwd, up))
    rot = N.identity(4, dtype=N.float32)
    rot[0,0:3] = rt
    rot[1,0:3] = up
    rot[2,0:3] = -fwd
    return N.dot(rot, trans)

def viewFromFrame(pos, fwd, rt, up):
    # quick inverse of translation+rotation
    trans = N.identity(4,dtype=N.float32)
    trans[0:3,3] = -pos[0:3]
    rot = N.identity(4,dtype=N.float32)
    rot[0,0:3] = rt[0:3]
    rot[1,0:3] = up[0:3]
    rot[2,0:3] = -fwd[0:3]
    return N.dot(rot, trans)

def modelFromFrame(pos, fwd, rt, up):
    # translation+rotation
    model = N.identity(4,dtype=N.float32)
    model[0:3,3] = pos[0:3]
    model[0:3,0] = rt[0:3]
    model[0:3,1] = up[0:3]
    model[0:3,2] = -fwd[0:3]
    return model
    
#creates translation matrix based
def translationMatrix(x,y,z):
    return N.array(((1, 0, 0, x),
                    (0, 1, 0, y),
                    (0, 0, 1, z),
                    (0, 0, 0, 1)), dtype = N.float32)

#takes a matrix, and sets its translation column
def setTranslation(m,x,y,z):
    m[:,:] = N.eye(4, dtype=N.float32)
    m[:,3] = (x,y,z,1)
    
#creates rotation around x matrix
def rotationXMatrix(angle):
    s = N.sin(angle)
    c = N.cos(angle)
    return N.array(((1.0, 0.0, 0.0, 0.0),
                    (0.0,   c,  -s, 0.0),
                    (0.0,   s,   c, 0.0),
                    (0.0, 0.0, 0.0, 1.0)), dtype = N.float32)

#takes a matrix, and inputs values in the rotation by x places
def setRotationX(m,angle):
    s = N.sin(angle)
    c = N.cos(angle)
    m[:,:] = N.eye(4, dtype=N.float32)
    m[1,1] = c
    m[2,2] = c
    m[1,2] = -s
    m[2,1] = s

#creates rotation around y matrix
def rotationYMatrix(angle):
    s = N.sin(angle)
    c = N.cos(angle)
    return N.array(((  c, 0.0,  -s, 0.0),
                    (0.0, 1.0, 0.0, 0.0),
                    (  s, 0.0,   c, 0.0),
                    (0.0, 0.0, 0.0, 1.0)), dtype = N.float32)

#takes a matrix, and inputs values in the rotation by y places
def setRotationY(m,angle):
    s = N.sin(angle)
    c = N.cos(angle)
    m[:,:] = N.eye(4, dtype=N.float32)
    m[0,0] = c
    m[2,2] = c
    m[0,2] = -s
    m[2,0] = s

#creates rotation around z matrix    
def rotationZMatrix(angle):
    s = N.sin(angle)
    c = N.cos(angle)
    return N.array(((  c,  -s, 0.0, 0.0),
                    (  s,   c, 0.0, 0.0),
                    (0.0, 0.0, 1.0, 0.0),
                    (0.0, 0.0, 0.0, 1.0)), dtype = N.float32)

#takes a matrix, and inputs values in the rotation by z places
def setRotationZ(m,angle):
    s = N.sin(angle)
    c = N.cos(angle)
    m[:,:] = N.eye(4, dtype=N.float32)
    m[0,0] = c
    m[1,1] = c
    m[0,1] = -s
    m[1,0] = s
    

#takes  buffer of verts of form:
#(x,y,z,w,nx,ny,nz,nw,u,v)
#nd transforms them based on a matrix    
def moveVerts(buff, matrix):
    buff = vec(buff)
    x = 0
    #step through buffer
    while x != N.size(buff):
        #pluck out vertex
        v = vec(buff[x:x+4])
        #transform vertex
        v = N.dot(matrix,v)
        #place vertex back in buff
        buff[x:x+4] = v
        #pluck out normal
        v = vec(buff[x+4:x+8])
        #transform normal
        v = N.dot(matrix,v)
        #place normal back in buff
        buff[x+4:x+8] = v
        #step to beginning of next vertex
        x = x+12
    return buff
        
def main():
    v = [0,1,2,1,4,5,6,7,8,9,10,11,12,1,14,15,16,17,18,19]
    m = [[1,0,0,1],[0,1,0,1],[0,0,1,1],[0,0,0,1]]
    v = moveVerts(v,m)
    print "hery"
    print v

if __name__ == "__main__":
    main()